const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const PORT = 4005;

app.use(bodyParser.json());

let cartItems = [];

app.get('/cart', (req, res) => {
  res.send(cartItems);
});

app.post('/cart', (req, res) => {
  const { name, price } = req.body;
  const newItem = { name, price };
  cartItems.push(newItem);
  res.send(newItem);
});

app.delete('/cart/:id', (req, res) => {
  const itemId = parseInt(req.params.id);
  cartItems = cartItems.filter(item => item.id !== itemId);
  res.send('Item deleted successfully');
});

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
